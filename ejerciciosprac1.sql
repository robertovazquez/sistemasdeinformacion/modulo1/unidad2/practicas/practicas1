﻿USE practica1;



-- 1 ejercicio

  SELECT * FROM emple;


  -- 2 ejercicio

    SELECT * FROM depart;

 -- 3 ejercicio

  SELECT DISTINCT e.apellido,e.oficio FROM emple e;

  -- 4 ejercicio

    SELECT DISTINCT d.loc,d.dept_no FROM depart d;


-- 5 ejercicio 
 
  
 SELECT DISTINCT d.loc,d.dept_no,d.dnombre FROM depart d;

-- 6 ejercicio

 SELECT DISTINCT COUNT(*) FROM emple e;
  

-- 7 ejercicio

 SELECT DISTINCT e.apellido,e.oficio,e.emp_no,e.dir,e.fecha_alt,e.salario,e.comision,e.dept_no FROM emple e ORDER BY  e.apellido ASC;

-- 8 ejercicio

SELECT DISTINCT e.apellido ,e.oficio,e.emp_no,e.dir,e.fecha_alt,e.salario,e.comision,e.dept_no FROM emple e ORDER BY e.apellido DESC;

-- 9 ejercicio

SELECT DISTINCT COUNT(*) FROM depart d;

-- 10 ejercicio

 SELECT (SELECT COUNT(*) FROM emple)+(SELECT COUNT(*) FROM depart)suma;


-- 11 ejercicio
SELECT DISTINCT e.apellido,e.oficio,e.emp_no,e.dir,e.fecha_alt,e.salario,e.comision, e.dept_no FROM emple e ORDER BY  e.dept_no DESC;

-- 12 ejercicio

SELECT DISTINCT e.apellido,e.oficio,e.emp_no,e.dir,e.fecha_alt,e.salario,e.comision,e.dept_no FROM emple e ORDER BY e.oficio ASC, e.dept_no DESC;

-- 13 ejercicio


SELECT DISTINCT e.apellido,e.oficio,e.emp_no,e.dir,e.fecha_alt,e.salario,e.comision,e.dept_no  FROM emple e ORDER BY  e.apellido ASC,e.dept_no DESC;


-- 14 ejercicio

  SELECT DISTINCT e.emp_no FROM emple e WHERE e.salario >2000;

  -- 15 ejercicio 
    
    SELECT DISTINCT e.emp_no,e.apellido FROM emple e WHERE e.salario <2000;

 -- 16 ejercicio

 SELECT DISTINCT e.apellido,e.oficio,e.emp_no,e.dir,e.fecha_alt,e.salario,e.comision,e.dept_no FROM emple e WHERE e.salario BETWEEN 1500 AND 2500;

  -- 17 ejercicio
 SELECT DISTINCT e.apellido,e.oficio,e.emp_no,e.dir,e.fecha_alt,e.salario,e.comision,e.dept_no FROM emple e WHERE e.oficio='ANALISTA';

-- 18 ejercicio 
 SELECT DISTINCT e.apellido,e.oficio,e.emp_no,e.dir,e.fecha_alt,e.salario,e.comision,e.dept_no FROM emple e WHERE e.oficio='ANALISTA' AND e.salario>2000;

-- 19 ejercicio

 SELECT DISTINCT e.apellido,e.oficio FROM emple e WHERE e.dept_no=20;

-- 20 ejercicio

SELECT DISTINCT COUNT(*) FROM emple e WHERE e.oficio='vendedor'; 

-- 21 ejercicio

 SELECT DISTINCT e.apellido,e.oficio,e.emp_no,e.dir,e.fecha_alt,e.salario,e.comision,e.dept_no FROM emple e WHERE e.apellido  LIKE 'm%' OR e.apellido LIKE 'n%' ORDER BY e.apellido ASC;


-- 22 ejercicio
SELECT DISTINCT e.apellido,e.oficio,e.emp_no,e.dir,e.fecha_alt,e.salario,e.comision,e.dept_no FROM emple e WHERE e.oficio='VENDEDOR' ORDER BY e.apellido ASC;

-- 23 ejercicio

  SELECT e.apellido FROM emple e WHERE e.salario=(SELECT MAX(e.salario) FROM emple e);

  -- 24 ejercicio

 SELECT DISTINCT e.apellido,e.oficio,e.emp_no,e.dir,e.fecha_alt,e.salario,e.comision,e.dept_no FROM emple e WHERE e.dept_no=10 AND e.oficio='ANALISTA'  ORDER BY e.apellido ASC, e.oficio ASC;



-- 25 ejercicio
SELECT MONTH(fecha_alt) FROM emple e;

-- 26 ejerccio

SELECT YEAR(fecha_alt) FROM emple e;

-- 27 ejercicio

SELECT DAY (fecha_alt) FROM emple e;

-- 28 ejercicio

SELECT DISTINCT e.apellido FROM emple e WHERE e.salario>2000 OR e.dept_no = 20;

-- 29 ejercicio

    SELECT DISTINCT e.apellido FROM emple e JOIN depart d ON e.dept_no = d.dept_no;


-- 30 ejercicio

      SELECT DISTINCT e.apellido,e.oficio,d.dnombre  FROM emple e JOIN depart d ON e.dept_no = d.dept_no;


  -- 31 ejercicio

  SELECT DISTINCT e.dept_no,COUNT(*) FROM emple e GROUP BY e.dept_no;

  -- 32 ejercicio

 SELECT dnombre,c2.Número_de_empleados 
    FROM 
    (SELECT dept_no,COUNT(emp_no)AS 'Número_de_empleados' 
      FROM emple 
      GROUP BY dept_no
     ) AS c2
    JOIN depart
    ON c2.dept_no=depart.dept_no ;




 -- 33 ejercicio

   SELECT e.apellido FROM emple e ORDER BY .e.apellido ASC , e.oficio ASC;


  -- 34 ejercicio

    SELECT e.apellido FROM emple e WHERE e.apellido LIKE 'A%';

    -- 35 ejercicio

    SELECT e.apellido FROM emple e WHERE e.apellido LIKE 'A%' OR e.apellido LIKE 'M%';

    -- 36 ejercicio

 SELECT DISTINCT e.apellido,e.oficio,e.emp_no,e.dir,e.fecha_alt,e.salario,e.comision,e.dept_no FROM emple e WHERE e.apellido NOT LIKE '%Z';


-- 37 ejerccio

 SELECT DISTINCT e.apellido,e.oficio,e.emp_no,e.dir,e.fecha_alt,e.salario,e.comision,e.dept_no FROM emple e WHERE e.apellido LIKE 'A%' AND e.oficio LIKE '%E%' ORDER BY  e.oficio DESC , e.salario DESC;





























 





